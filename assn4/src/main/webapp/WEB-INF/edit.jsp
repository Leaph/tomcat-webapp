<%--
  Created by IntelliJ IDEA.
  User: tyeon
  Date: 2/12/17
  Time: 10:03 PM
  To change this template use File | Settings | File Templates.
--%>
<html>
<p>
<h2>Edit User</h2>
<p>${error}</p>
<form action="/manage" method="POST">
    <input type="hidden" name="type" value="edit"/>
    <input type="hidden" name="user_id" value="${user_id}">
    Username:<br/>
    <input type="text" name="username"/>
    <br/>
    Role:<br/>
    <input type="text" name="role"/>
    <br/>
    <input type="submit" value="Edit">
</form>
<form action="/" method="GET">
    <input type="submit" value="Cancel">
</form>
</body>
</html>
