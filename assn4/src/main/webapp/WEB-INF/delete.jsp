<%--
  Created by IntelliJ IDEA.
  User: tyeon
  Date: 2/12/17
  Time: 10:03 PM
  To change this template use File | Settings | File Templates.
--%>
<html>
<p>
<h2>Are you sure you want to delete ${user} ?</h2>
<form action="/manage" method="POST">
    <input type="hidden" name="type" value="delete"/>
    <input type="hidden" name="user" value="${user}">
    <input type="submit" value="Delete">
</form>
<form action="/" method="GET">
    <input type="submit" value="Cancel">
</form>
</body>
</html>
