<%--
  Created by IntelliJ IDEA.
  User: tyeon
  Date: 2/12/17
  Time: 10:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<body>
<div class="col-md-12">
    <div class="row">
        <div class="col-md-12">
            <div>
                <h1>Hello, <%=session.getAttribute("username")%>!</h1>
                <h2>Your role: <%=session.getAttribute("role")%></h2>
                <h3 class="pull-left">Users</h3>
                <form action="/manage" method="GET">
                    <input type="hidden" name="type" value="create"/>
                    <button type="submit">Add User</button>
                </form>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table" style="border: 1px solid black; width: 500px;">
                            <thead>
                            <tr>
                                <th style="width: 40%;">Username</th>
                                <th style="width: 30%;">Role</th>
                                <th style="width: 30%;">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <%
                                try{
                                    Statement statement;
                                    Connection connection;
                                    connection = DriverManager.getConnection("jdbc:mysql://localhost/ooc_test?"
                                            + "user=ooc&password=oocpass");
                                    statement = connection.createStatement();
                                    ResultSet rs =statement.executeQuery("SELECT * FROM ooc_test.user;");
                                    while(rs.next()){
                            %>
                            <tr>
                                <td>
                                    <%= rs.getString("username") %>
                                </td>
                                <td>
                                    <%= rs.getString("role") %>
                                </td>
                                <td>
                                    <span>
                                        <form action="/manage" method="GET">
                                            <input type="hidden" name="type" value="edit"/>
                                            <input type="hidden" name="user_id" value="<%= rs.getLong("id")%>">
                                            <input type="submit" value="Edit">
                                        </form>
                                        <% if (!rs.getString("username").equals(session.getAttribute("username"))) {%>
                                        <form action="/manage" method="GET">
                                            <input type="hidden" name="type" value="delete"/>
                                            <input type="hidden" name="user" value="<%= rs.getString("username")%>">
                                            <input type="submit" value="Delete">
                                        </form>
                                        <%}%>
                                    </span>
                                </td>
                            </tr>
                            <%}%>
                            <%
                                    rs.close();
                                    statement.close();
                                    connection.close();
                                }
                                catch(Exception e)
                                {
                                    e.printStackTrace();
                                }
                            %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="/logout" method="GET">
    <input type="submit" value="Logout">
</form>
</body>
</html>
