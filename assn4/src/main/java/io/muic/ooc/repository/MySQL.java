package io.muic.ooc.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class MySQL {

    public enum Columns {
        id, username, password, role;
    }

    private final String jdbcDriverStr;
    private final String jdbcURL;

    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;
    private PreparedStatement preparedStatement;

    public MySQL(String jdbcDriverStr, String jdbcURL) {
        this.jdbcDriverStr = jdbcDriverStr;
        this.jdbcURL = jdbcURL;
    }

    public Map<String, String> getLogins(Columns column) {
        Map<String, String> logins = new HashMap<>();
        try {
            Class.forName(jdbcDriverStr);
            connection = DriverManager.getConnection(jdbcURL);
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM ooc_test.user;");
            while (resultSet.next()) {
                String username = resultSet.getString(Columns.username.toString());
                String password = resultSet.getString(column.toString());
                logins.put(username, password);
            }
        } catch (Exception e) {

        } finally {
            close();
            return logins;
        }
    }

    public boolean addUser(String username, String password, String role) {
        try {
            Class.forName(jdbcDriverStr);
            connection = DriverManager.getConnection(jdbcURL);
            statement = connection.createStatement();
            statement.executeUpdate("INSERT INTO `user`(username, password, role) VALUE ('"+username+"','"+password+"','"+role+"')");
            return true;
        } catch (Exception e) {
            System.out.println("DUPLICATE USERNAME!");
            return false;
        } finally {
            close();
        }
    }

    public boolean editUser(long id, String username, String role) {
        try {
            Class.forName(jdbcDriverStr);
            connection = DriverManager.getConnection(jdbcURL);
            statement = connection.createStatement();
            statement.executeUpdate("UPDATE `user` SET username = '"+username+"', role = '"+role+"' WHERE id = '"+id+"'");
            return true;
        } catch (Exception e) {
            System.out.println("USERNAME NONEXISTENT!");
            return false;
        } finally {
            close();
        }
    }

    public boolean deleteUser(String username) {
        try {
            Class.forName(jdbcDriverStr);
            connection = DriverManager.getConnection(jdbcURL);
            statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM `user` WHERE username = '"+username+"'");
            return true;
        } catch (Exception e) {
            System.out.println("USERNAME NONEXISTENT!");
            return false;
        } finally {
            close();
        }
    }

    private void close() {
        try {
            if (resultSet != null) resultSet.close();
            if (statement != null) statement.close();
            if (connection != null) connection.close();
        } catch (Exception e) {
        }
    }
}
