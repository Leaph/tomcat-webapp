package io.muic.ooc;

import com.ja.security.PasswordHash;
import io.muic.ooc.repository.MySQL;
import io.muic.ooc.service.SecurityService;
import io.muic.ooc.service.UserService;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import javax.servlet.ServletException;
import java.io.File;

import static io.muic.ooc.service.UserService.MYSQL_DRIVER;
import static io.muic.ooc.service.UserService.MYSQL_URL;

/**
 * Hello world!
 *
 */
public class WebApp {


    public static void main(String[] args) throws Exception {

        String docBase = "src/main/webapp/";
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(80);

        MySQL mySQL = new MySQL(MYSQL_DRIVER, MYSQL_URL);

        UserService userService = new UserService();
        userService.setMysql(mySQL);
        userService.addUser("admin", new PasswordHash().createHash("pass"), "admin");

        SecurityService securityService = new SecurityService();
        securityService.setUserService(userService);

        ServletRouter servletRouter = new ServletRouter();
        servletRouter.setSecurityService(securityService);
        servletRouter.setUserService(userService);

        Context ctx;
        try {
            ctx = tomcat.addWebapp("/", new File(docBase).getAbsolutePath());
            servletRouter.init(ctx);
            tomcat.start();
            tomcat.getServer().await();
        } catch (ServletException | LifecycleException ex) {
            ex.printStackTrace();
        }
    }
}
