package io.muic.ooc;

import io.muic.ooc.service.SecurityService;
import io.muic.ooc.service.UserService;
import io.muic.ooc.servlet.*;
import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.util.descriptor.web.ErrorPage;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by tyeon on 2/15/17.
 */
public class ServletRouter {

    private SecurityService securityService;
    private UserService userService;
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }
    public void setUserService(UserService userService) { this.userService = userService; }

    public void init(Context ctx) {
        initHome(ctx);
        initLogin(ctx);
        initLogout(ctx);
        initAddUser(ctx);
        initFilter(ctx);
    }

    private void initHome(Context ctx) {
        HomeServlet homeServlet = new HomeServlet();
        homeServlet.setSecurityService(securityService);
        Tomcat.addServlet(ctx, "HomeServlet", homeServlet);
        ctx.addServletMapping("/index.jsp", "HomeServlet");
    }

    private void initLogin(Context ctx) {
        LoginServlet loginServlet = new LoginServlet();
        loginServlet.setSecurityService(securityService);
        Tomcat.addServlet(ctx, "LoginServlet", loginServlet);
        ctx.addServletMapping("/login", "LoginServlet");
    }

    private void initLogout(Context ctx) {
        LogoutServlet logoutServlet = new LogoutServlet();
        logoutServlet.setSecurityService(securityService);
        Tomcat.addServlet(ctx, "LogoutServlet", logoutServlet);
        ctx.addServletMapping("/logout", "LogoutServlet");
    }

    private void initAddUser(Context ctx) {
        UserServlet userServlet = new UserServlet();
        userServlet.setSecurityService(securityService);
        userServlet.setUserService(userService);
        Tomcat.addServlet(ctx, "UserServlet", userServlet);
        ctx.addServletMapping("/manage", "UserServlet");
    }

    private void initFilter(Context ctx) {
        ErrorPage errorPage = new ErrorPage();
        errorPage.setErrorCode(HttpServletResponse.SC_NOT_FOUND);
        errorPage.setLocation("/");
        ctx.addErrorPage(errorPage);
    }
}
