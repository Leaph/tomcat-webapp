package io.muic.ooc.servlet;

import io.muic.ooc.service.SecurityService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by tyeon on 2/15/17.
 */
public class HomeServlet extends HttpServlet {

    private SecurityService securityService;

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean authorized = securityService.isAuthorized(request);
        if (authorized) {
            // do MVC in here
            RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/index.jsp");
            rd.include(request, response);
        } else {
            response.sendRedirect("/login");
        }
    }
}
