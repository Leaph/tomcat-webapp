package io.muic.ooc.servlet;

import io.muic.ooc.service.SecurityService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by tyeon on 2/15/17.
 */
public class ErrorServlet extends HttpServlet {
    private SecurityService securityService;

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean authorized = securityService.isAuthorized(request);
        if (authorized) {
            response.sendRedirect("/");
        } else {
            RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/login");
            rd.include(request, response);
        }
    }
}
