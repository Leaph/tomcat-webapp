package io.muic.ooc.servlet;

import io.muic.ooc.repository.MySQL;
import io.muic.ooc.service.SecurityService;
import io.muic.ooc.service.UserService;
import org.apache.commons.lang.StringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by tyeon on 2/16/17.
 */
public class UserServlet extends HttpServlet {

    private SecurityService securityService;
    private UserService userService;
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }
    public void setUserService(UserService userService) { this.userService = userService; }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (securityService.isAuthorized(request)) {
            String type = request.getParameter("type");
            if (type.equals("create")) {
                RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/add.jsp");
                rd.include(request, response);
            } else if (type.equals("edit")) {
                request.setAttribute("user_id", request.getParameter("user_id"));
                RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/edit.jsp");
                rd.include(request, response);
            } else if (type.equals("delete")) {
                request.setAttribute("user", request.getParameter("user"));
                RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/delete.jsp");
                rd.include(request, response);
            } else {
                response.sendRedirect("/");
            }
        } else {
            response.sendRedirect("/login");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (securityService.isAuthorized(request)) {
            String type = request.getParameter("type");
            if (type.equals("create")) add(request, response);
            else if (type.equals("edit")) edit(request, response);
            else if (type.equals("delete")) delete(request, response);

        } else {
            RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/login.jsp");
            rd.include(request, response);
        }
    }

    private void add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = securityService.hash(request.getParameter("password"));
        String role = request.getParameter("role");

        boolean success = userService.addUser(username, password, role);

        if (success) {
            response.sendRedirect("/");
        } else {
            String error = "Username or password is invalid. Make sure your username is unique!";
            request.setAttribute("error", error);
            RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/add.jsp");
            rd.include(request, response);
        }
    }

    private void edit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long id = Long.parseLong(request.getParameter("user_id"));
        String username = request.getParameter("username");
        String role = request.getParameter("role");

        boolean success = userService.editUser(id, username, role);

        if (success) {
            if (id != Long.parseLong(request.getSession().getAttribute("id").toString())) {
                response.sendRedirect("/");
            } else {
                request.getSession().setAttribute("user_id", id);
                request.getSession().setAttribute("username", username);
                request.getSession().setAttribute("role", role);
                RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/index.jsp");
                rd.include(request, response);
            }
        } else {
            String error = "Make sure the new username is unique!";
            request.setAttribute("error", error);
            RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/edit.jsp");
            rd.include(request, response);
        }
    }

    private void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = (String) request.getSession().getAttribute("username");
        String userForDelete = request.getParameter("user");
        userService.deleteUser(username, userForDelete);
        response.sendRedirect("/");
    }

}
