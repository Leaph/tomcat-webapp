package io.muic.ooc.service;

import io.muic.ooc.repository.MySQL;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

/**
 * Created by tyeon on 2/15/17.
 */
public class UserService {

    public static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
    public static final String MYSQL_URL = "jdbc:mysql://localhost/ooc_test?"
            + "useSSL=false&user=ooc&password=oocpass";

    private MySQL dao;
    public void setMysql(MySQL mysql) {
        this.dao = mysql;
    }

    public Map<String, String> getUsers(MySQL.Columns column) {
        return dao.getLogins(column);
    }

    public boolean addUser(String username, String password, String role) {
        if (!StringUtils.isBlank(username) && !StringUtils.isBlank(password)) {
            return dao.addUser(username, password, role);
        }
        return false;
    }

    public boolean editUser(Long id, String username, String role) {
        if (id != null) {
            return dao.editUser(id, username, role);
        }
        return false;
    }

    public boolean deleteUser(String username, String userForDelete) {
        if (!StringUtils.isBlank(userForDelete) && !StringUtils.equals(username, userForDelete)) {
            return dao.deleteUser(userForDelete);
        }
        return false;
    }
}
