package io.muic.ooc.service;

import com.ja.security.PasswordHash;
import io.muic.ooc.repository.MySQL;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by tyeon on 2/15/17.
 */
public class SecurityService {

    private UserService userService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public boolean isAuthorized(HttpServletRequest request) {
        String username = (String) request.getSession().getAttribute("username");
        return username != null && userService.getUsers(MySQL.Columns.password).containsKey(username);
    }

    public boolean authenticate(HttpServletRequest request) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) return false;

        String passwordInDB = userService.getUsers(MySQL.Columns.password).get(username);
        boolean isMatched = false;
        try {
            isMatched = new PasswordHash().validatePassword(password, passwordInDB);
        } catch (Exception e) {

        }
        if (isMatched) {
            request.getSession().setAttribute("id", userService.getUsers(MySQL.Columns.id).get(username));
            request.getSession().setAttribute("username", username);
            request.getSession().setAttribute("role", userService.getUsers(MySQL.Columns.role).get(username));
        }
        return isMatched;
    }

    public void logout(HttpServletRequest request) {
        request.getSession().invalidate();
    }

    public String hash(String password) {
        try {
            String passwordHash = new PasswordHash().createHash(password);
            return passwordHash;
        } catch (Exception e) {
            return null;
        }
    }
}
